﻿using System.ComponentModel.DataAnnotations;

namespace _2180604808_NguyenDuongTanDung_KiemTra.Models
{
    public class Transaction
    {
        [Key]
        public int TransactionalId { get; set; }
        [Required]
        public Employee? Employee { get; set; }
        
        public int EmployeeId { get; set; }
        [Required]
        public Customer? Customer { get; set; }

        public int CustomerId { get; set; }

        public string Name { get; set; }

    }
}
