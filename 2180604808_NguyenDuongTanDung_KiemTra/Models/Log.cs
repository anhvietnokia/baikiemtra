﻿using System.ComponentModel.DataAnnotations;

namespace _2180604808_NguyenDuongTanDung_KiemTra.Models
{
    public class log
    {
        [Key]
        public int LogsId { get; set; }
        [Required]
        public Transaction? Transaction { get; set; }
        public int TransactionalId { get; set; }

        public DateOnly LoginDate { get; set; }

        public TimeOnly LoginTime { get; set; }

        List<Report> Reports { get; set; }
    }
}
