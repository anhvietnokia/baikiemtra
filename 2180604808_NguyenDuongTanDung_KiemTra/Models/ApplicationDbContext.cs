﻿using Microsoft.EntityFrameworkCore;

namespace _2180604808_NguyenDuongTanDung_KiemTra.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)  
        {
        }
        public DbSet<Account> Accounts
        {
            get; set;
        }
        public DbSet<Customer> Customers
        {
            get; set;
        }
        public DbSet<log> Logs
        {
            get; set;
        }
        public DbSet<Report> Reports
        {
            get; set;
        }
        public DbSet<Transaction> Transactions
        {
            get; set;
        }
        public DbSet<Employee> Employees
        {
            get; set;
        }

    }
}