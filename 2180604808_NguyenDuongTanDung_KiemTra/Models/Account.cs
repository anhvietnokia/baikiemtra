﻿using System.ComponentModel.DataAnnotations;

namespace _2180604808_NguyenDuongTanDung_KiemTra.Models
{
    public class Account
    {

        [Key]
        public int AccountId { get; set; }
        [Required]
        public int CustomerID { get; set; }
        
        public Customer? Customer { get; set; }
        [Required, StringLength(255)]
        public string AccountName { get; set; }

    }   
}
