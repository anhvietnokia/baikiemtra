﻿using System.ComponentModel.DataAnnotations;

namespace _2180604808_NguyenDuongTanDung_KiemTra.Models
{
    public class Employee
    {
        [Key]
        public int EmployeeId { get; set; }
        [Required, StringLength(255)]
        public string FirstName { get; set; }
        [Required, StringLength(255)]
        public string LastName { get; set; }

        public string ContactandAdress { get; set; }

        public string UsernameandPassword { get; set; }
    }
}
