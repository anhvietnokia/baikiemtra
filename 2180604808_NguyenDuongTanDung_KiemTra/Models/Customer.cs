﻿using System.ComponentModel.DataAnnotations;

namespace _2180604808_NguyenDuongTanDung_KiemTra.Models
{
    public class Customer
    {
        [Key]
        public int CustomerId { get; set; }
        [Required,StringLength(100)]       
        public string FirstName { get; set; }
        [Required, StringLength(100)]
        public string LastName { get; set; }
      
        public string ContactandAddress { get; set; }
    
        public string UserName { get; set; }
    

        public string Password { get; set; }

        
    }
}
