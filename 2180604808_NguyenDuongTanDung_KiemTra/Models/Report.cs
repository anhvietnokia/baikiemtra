﻿using System.ComponentModel.DataAnnotations;

namespace _2180604808_NguyenDuongTanDung_KiemTra.Models
{
    public class Report
    {
        [Key]
        public int ReportsId { get; set; }
        [Required]

        public Account? Account { get; set; }
        [Required]
        public int AccountId { get; set; }
        public log? logs { get; set; }
        [Required]
        public int LogsId { get; set; }

        public string ReportName { get; set; }

        public DateTime ReportDate { get; set; }

        public Transaction? Transaction { get; set; }
        [Required]
        public int TransactionalId { get; set; }


    }
}
